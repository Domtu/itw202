import React, { useState } from 'react';
import { Button, StyleSheet, Text, TextInput, View, FlatList } from 'react-native';

import { AspirationItem } from './components/AspirationItem';
import AspirationInput from './components/AspirationInput';

export default function App() {
  const [courseAspirations, setCourseAspirations] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const addAspirationHandler = aspirationTitle => {
    // console.log(enteredAspiration);
    // setCourseAspirations([...courseAspirations, enteredAspiration]);
    setCourseAspirations(currentAspirations => [
      ...courseAspirations, 
      {key: Math.random().toString(), value: aspirationTitle }
    ])
    setIsAddMode(false)
  };
  const removeAspirationHandeler = aspirationKey => {
    setCourseAspirations(currentAspirations => {
      return currentAspirations.filter((aspiration) => aspiration.key !== aspirationKey)
    })
  }
  return (
    <View style={styles.screen}>
      <Button title='Add New Aspiration' onPress={() => setIsAddMode(true)} />
      <AspirationInput visible={isAddMode} onAddAspiration = {addAspirationHandler}/>  
      <FlatList
        data={courseAspirations}
        renderItem = {itemData => 
          <AspirationItem id={itemData.item.key} onDelete={removeAspirationHandeler} title = {itemData.item.value}/>
        }
        />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50
  }
});
