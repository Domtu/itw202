import React, { useState } from "react";
import { StyleSheet, View, TextInput, Button, Modal } from "react-native";

const AspirationInput = (props) => {
    const [enteredAspiration, setEnteredAspiration] = useState('');

    const clearInput = (enteredText) => {
        setEnteredAspiration();
    }

    const AspirationInputHandler = (enteredText) => {
        setEnteredAspiration(enteredText);
    };
    
    return (
        <Modal visible = {props.visible} animationType="slide">
        <View style={styles.inputContainer}>
            <TextInput
            placeholder="My Aspiration from this model"
            style={styles.input}
            onChangeText={AspirationInputHandler}
            value={enteredAspiration}
            />
            <Button
                title="ADD"
                onPress={() => props.onAddAspiration(enteredAspiration)} />
            <Button title="CANCEL" onPress={clearInput} />
        </View>
        </Modal>
    )
}
export default AspirationInput
const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    input: {
        width: '58%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        margin: 15,
    }
});