import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyComponent from './component/MyComponent';
import App from './component/App';
export default function App() {
  return (
    <View style={styles.container}>
      <MyComponent></MyComponent>
      <app></app>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
