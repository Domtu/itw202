import React from "react";
import { View, StyleSheet, Text} from "react-native";

const MyComponent = () => {
    const greeting = 'sonam wangmo'
    const greeting1 = <Text>jigme wangmo</Text>
    return(
        <View>
            <Text>This is a demol of JSX</Text>
            <Text>Hi There!!! {greeting}</Text>
            {greeting1}
        </View>
    )
};
const styles =StyleSheet.create({
    textStyle:{
        fontSize:24
    }
})


export default MyComponent;
