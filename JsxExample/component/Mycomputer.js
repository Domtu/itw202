import React from "react";
import {StyleSheet, Text, View, TextInput, Button}from 'react-native';

export default function App(){
    return (
        <View style = {{ flex:1, justifyContent:'center',alignItems:'center',backgroundColor:'#F5FCFF'}}>
            <Text style ={{fontSize:20, textAlign:'center', margin:10}}>
                welcome to React Native!
            </Text>
        </View>
    )
}
const style = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        background:'#F5FCFF'
    },
    welcome:{
        fontSize:20,
        textAlign:'center',
        margin:10
    };
})