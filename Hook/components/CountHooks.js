
import React, {useState} from "react";
import{View,Text,Button}from 'react-native';
const CountHook = () =>{
    const [count, setCount] =useState(0);
    return(
        <View>
            <Text>
                You Clicked {count} times
            </Text>
            < Button
            onPress={()=>setCount(count + 1)}
            title="Click me!"
            />
        </View>
    )

}
export default CountHook;
