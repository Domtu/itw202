import { StyleSheet, Text, View, Image  } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { usestate } from "react";
// import RegistrationScreen from "./src/screens/RegistrationScreen/RegistrationScreen";
// import LoginScreen from "./src/screens/Users/UserLoginScreen/UserLoginScreen";
// import { HomePage } from "./src/screens/ResetScreen";
// import ResetPasswordScreen from "./src/screens/ResetScreen/ResetPasswordScreen";
// import {  ResetScreen, HomeScreen } from "./src/screens/ResetScreen";
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
// import DrawerContent from './src/Components/DrawerContent';
// import addRecipe from "./src/screens/ResetScreen/AddRecipe";
// import First from "./src/screens/ResetScreen/FirstPage"
import DrawerContent from './component/Components/DrawerContent';
import UserRegistrationScreen from "../users/screens/Users/UserRegistrationScreen/UserRegistrationScreen";
import UserLoginScreen from "../users/screens/Users/UserLoginScreen/UserLoginScreen";
import UserResetPasswordScreen from "../users/screens/Users/UserResetScreen/UserResetPasswordScreen";

import Page from './screens/Users/UserHomepage'
import Add from './screens/Users/AddPage'
import AddRecipe  from "./screens/Users/UserAddRecipe";
import Update from "../users/screens/Users/Update"
import LoginHomepage from "../users/screens/Users/LoginHomepage"
import AboutPage from "../users/screens/Users/AboutPage"
import AddBreakfast from "./screens/Users/AddBreakfast"
import AddLunch from "./screens/Users/AddLunch"
import AddDinner from "./screens/Users/AddDinner"
import AddSnacks from "./screens/Users/AddSnacks"
import AddDrinks from "./screens/Users/AddDrinks"
import Category from "./screens/Users/Category";
import BreakFast from "./screens/Users/BreakFast";
import UserBreakFast from "./screens/Users/UserBreakFast";
import Action  from "./component/Components/Action"
import BreakfastItem from './screens/Users/BreakfastItem'
import UserBreakfastItem from './screens/Users/UserBreakfastItem'
import Lunch from './screens/Users/Lunch'
import UserLunch from './screens/Users/UserLunch'
import LunchItem from './screens/Users/LunchItem'
import UserLunchItem from './screens/Users/UserLunchItem'
import Dinner from './screens/Users/Dinner'
import UserDinner from './screens/Users/UserDinner'
import DinnerItem from './screens/Users/DinnerItem'
import UserDinnerItem from './screens/Users/UserDinnerItem'
import Snacks from './screens/Users/Snacks'
import UserSnacks from './screens/Users/UserSnacks'
import SnacksItem from './screens/Users/SnacksItem'
import UserSnacksItem from './screens/Users/UserSnacksItem'
import Drinks from './screens/Users/Drinks'
import UserDrinks from './screens/Users/UserDrinks'
import DrinksItem from './screens/Users/DrinksItem'
import UserDrinksItem from './screens/Users/UserDrinksItem'
import User from './screens/Users/User'
import CommentCategory from './screens/CommentCategory'
import BreakfastComment from './screens/BreakfastComment'
import LunchComment from './screens/LunchComment'
import DinnerComment from './screens/DinnerComment'
import SnacksComment from './screens/SnacksComment'
import DrinksComment from './screens/DrinksComment'
import Rating from './screens/Rating'
import Logo from './screens/Logo'


const Stack = createNativeStackNavigator();
 const Drawer =createDrawerNavigator();
 



export default function App() { 
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Logo'
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="UserLoginScreen" component={UserLoginScreen} />      
        <Stack.Screen name="UserRegistrationScreen" component={UserRegistrationScreen} />
        <Stack.Screen name="ResetPasswordScreen" component={UserResetPasswordScreen} /> 
        <Stack.Screen name="Page" component={Page} />
        <Stack.Screen name="AddPage" component={Add} />
        <Stack.Screen name="UserAddRecipe" component={AddRecipe} />
        <Stack.Screen name="userUpdate" component={Update} />
        <Stack.Screen name="LoginHomepage" component={DrawerNavigator} />
        <Stack.Screen name="AboutPage" component={AboutPage} />
        <Stack.Screen name="AddBreakfast" component={AddBreakfast} />
        <Stack.Screen name="AddLunch" component={AddLunch} />
        <Stack.Screen name="AddDinner" component={AddDinner} />
        <Stack.Screen name="AddSnacks" component={AddSnacks} />
        <Stack.Screen name="AddDrinks" component={AddDrinks} />
        <Stack.Screen name="Category" component={Category} />
        <Stack.Screen name="BreakFast" component={BreakFast} />
        <Stack.Screen name="UserBreakFast" component={UserBreakFast} />
        <Stack.Screen name="BreakfastItem" component={BreakfastItem} />
        <Stack.Screen name="UserBreakfastItem" component={UserBreakfastItem} />
        <Stack.Screen name="Lunch" component={Lunch} />
        <Stack.Screen name="UserLunch" component={UserLunch} />
        <Stack.Screen name="LunchItem" component={LunchItem} />
        <Stack.Screen name="UserLunchItem" component={UserLunchItem} />
        <Stack.Screen name="Dinner" component={Dinner} />
        <Stack.Screen name="UserDinner" component={UserDinner} />
        <Stack.Screen name="DinnerItem" component={DinnerItem} />
        <Stack.Screen name="UserDinnerItem" component={UserDinnerItem} />
        <Stack.Screen name="Snacks" component={Snacks} />
        <Stack.Screen name="UserSnacks" component={UserSnacks} />
        <Stack.Screen name="SnacksItem" component={SnacksItem} />
        <Stack.Screen name="UserSnacksItem" component={UserSnacksItem} />
        <Stack.Screen name="Drinks" component={Drinks} />
        <Stack.Screen name="UserDrinks" component={UserDrinks} />
        <Stack.Screen name="DrinksItem" component={DrinksItem} />
        <Stack.Screen name="UserDrinksItem" component={UserDrinksItem} />
        <Stack.Screen name="User" component={User} />
        <Stack.Screen name="BreakfastComment" component={BreakfastComment} />
        <Stack.Screen name="CommentCategory" component={CommentCategory} />
        <Stack.Screen name="LunchComment" component={LunchComment} />
        <Stack.Screen name="DinnerComment" component={DinnerComment} />
        <Stack.Screen name="SnacksComment" component={SnacksComment} />
        <Stack.Screen name="DrinksComment" component={DrinksComment} />
        <Stack.Screen name="Rating" component={Rating} />
        <Stack.Screen name="Logo" component={Logo} />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const DrawerNavigator =() =>{
  return(
    
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='Home' component={LoginHomepage}/>
      

    </Drawer.Navigator>
    
    
    
  )
}

