import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
import Action from "../../component/Components/Action";
import firebase from '../../component/Firebase/config';

export default function UserBreakfast ({navigation}) {
  // const [tittle, setTittle] = useState(''); 
  const [breakfast, setBreakfast] = useState([]);
  const tittleRef = firebase.firestore().collection('breakfast'); 

  useEffect(() => {

    tittleRef
        .orderBy('createdAt', 'desc')
        // .where ('username','==','Domtu')
        .onSnapshot(
            querySnapshot => {
                const newBreakfast = []
                querySnapshot.forEach(doc => {
                    const tittle = doc.data()
                    tittle.id = doc.id
                    newBreakfast.push(tittle)
                });
                setBreakfast(newBreakfast)
            },
            error => {

                console.error(error);
            }
        )
}, []);

const renderTittle = ({ item }) => {
  return (

      <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  )
}
return (

  <SafeAreaView>
   <View style={{backgroundColor:'#CFECEC', flexDirection:'row'}}>
     
      
        <TouchableOpacity onPress={()=>navigation.replace("LoginHomepage")}>
           
           <Image source={require('../../assets/assets/back.png')} style={{width:20,height:20,marginLeft:20,marginTop:40}}></Image>            
        </TouchableOpacity>
        <Text style={{marginLeft:110,marginTop:40, fontSize:17,color:'#05445E'}}>Breakfast/comments</Text>
        </View>
    
    <View style={{height:'100%'}}>

    {breakfast.length > 0 && (
              <View style={styles.listContainer}>
                  <FlatList
                      data={breakfast}
                      // numColumns={2}
                      horizontal={false}
                      ItemSeparatorComponent={() => <View style={{margin: 4}}/>}
                      renderItem={({ item }) => {
                        return(
                          <View>
                           
                           <View key={item.id} style={styles.place} >
                           <Text style={{marginTop:20, color:'black',marginLeft:50,fontSize:15}}>{item.comment}</Text>
                           
                          
                           </View>
                           </View>
                         )
                       }} />
                
              </View>
          )}
    </View>
    <View>
    
    </View>
  </SafeAreaView>
)
}

const styles = StyleSheet.create({
  container: {
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '100%',
    height: 200,
    marginLeft:20,
    overflow:'hidden',
  },
  text: {
    color: "white",
    fontSize: 28,
    lineHeight: 84,
    fontWeight: "bold",
    marginTop:130
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  tittleContainer: {
    borderBottomColor: 'black',
    borderWidth: 1,
    flex:1 ,
    flexDirection: 'column',
    height: 100,
    width: 100,
    margin: 5,
    backgroundColor: 'blue'
  },
  tittleText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  listContainer: {
    height:'100%',
    width: 400,
    flexDirection: 'row',
    
  
    
  },
  place:{
  
  }
  
});



