import React, {useState}from 'react';
import {View, StyleSheet,Text, Alert,ScrollView} from 'react-native';
import Header from '../../../component/Components/Header';
import Textinput from '../../../component/Components/TextInput'
import Button from '../../../component/Components/Button';
import GoBack from '../../../component/Components/BackButton';
import { TouchableOpacity } from 'react-native';
import { theme } from '../../../core/helpers/theme';
import { Formik } from 'formik';
import * as yup from 'yup';
import 'react-native-gesture-handler';
import Firebase from '../../../component/Firebase/config';

const reviewSchema = yup.object({
    username:
     yup
     .string()
     .required('Username is Required')
     .min(4),
    email:
    yup
    .string()
    .email('Please Enter Valid Email')
    .required('Email is Required'),
   
    password: 

    yup
    .string()
    .matches(/\w*[a-z]\w*/,  "Password must have a small letter")
    .matches(/\w*[A-Z]\w*/,  "Password must have a capital letter")
    .matches(/\d/, "Password must have a number")
    .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
    .min(8, ({ min }) => `Password must be at least ${min} characters`)
    .required('Password is required'), 

    Confirmpassword: 
    yup
    .string()
    .oneOf([yup.ref('password')], 'Password do not match')
    .required("Confirmpassword is required"),
  })

export default function RegistrationScreen({navigation}){
    

    const [emailError, setEmailError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)

    
    const createUser = async (values) => {
        let ref1 = Firebase.database().ref().child('users').push()
        ref1.set(values)
        Firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
        .then((userCredential) => {
          // Signed in 
          var user = userCredential.user;
          var obj = {uid: user.uid, ...values}


          Alert.alert('Successful Registration!!!')
          navigation.navigate('UserLoginScreen')      
    
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorMessage)
          var error = errorMessage.includes("Password")
          if(error){
            setPasswordError(errorMessage)        
            setEmailError(null)
            console.log(error)
          } else{
            setPasswordError(null)        
            setEmailError(errorMessage)
            console.log(error)
          }
        });
      }  
    return (
         <ScrollView>
           <View style={{marginLeft:20,width:'90%'}}>
            <View style={{marginLeft:100,marginTop:60}}>
            <Header>Sign Up</Header>
            </View>
          
            <View style={styles.container}>
            <Formik
        validationSchema = {reviewSchema}
        initialValues={{
          username: '',
          email: '',
          password: '',
          Confirmpassword: '',
          
        }}
        onSubmit = {(values,actions) => {
        // console.log(values)
        actions.resetForm()
        createUser(values)
        // console.log('My Id -->', uid)
        // navigation.navigate('UploadPicture')  
        }}
      >
      {(props)=>(
        <View>
          <Textinput
            label='Username'
            onChangeText = {props.handleChange('username')}
            value = {props.values.username}      
          />
          <Text style={styles.errorText}>{props.touched.username && props.errors.username}</Text>

          <Textinput
            label='Email'
            onChangeText = {props.handleChange('email')}
            value = {props.values.email}
            onBlur = {props.handleBlur('email')}            
          />
          {emailError ? 
            <Text style={styles.errorText}>{emailError}</Text>
            :null
          }
          <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>

          <Textinput
            label='Password'
            onChangeText = {props.handleChange('password')}
            value = {props.values.password}
            onBlur = {props.handleBlur('password')}
            secureTextEntry
          />
          {passwordError ? 
            <Text style={styles.errorText}>{passwordError}</Text>
            :null
          }
          <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>

          <Textinput
            label='Confirmpassword'
            onChangeText = {props.handleChange('Confirmpassword')}
            value = {props.values.Confirmpassword}
            onBlur = {props.handleBlur('Confirmpassword')}
            secureTextEntry
          />
          {passwordError ? 
            <Text style={styles.errorText}>{ConfirmpasswordError}</Text>
            :null
          }
          <Text style={styles.errorText}>{props.touched.Confirmpassword && props.errors.Confirmpassword}</Text>
          <View style={styles.row}>
            <Text>I agree with </Text>
            <TouchableOpacity onPress={()=>navigation.replace('TermsScreen')}>
              <Text style={styles.terms}>Terms and Policy</Text>
            </TouchableOpacity>
          </View>
          <Button 
            mode='contained'
            onPress={props.handleSubmit}>Create Account</Button>
           <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity onPress={()=>navigation.replace("UserLoginScreen")}>
                    <Text style={styles.link}>  Login</Text>
                </TouchableOpacity>
            </View> 
      </View>
    )}    
    </Formik>
    </View>
    </View>
    </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
      alignSelf:'center',
      width:'100%',
      marginTop:40         
    },
    row:{
        flexDirection:'row',
        marginTop:4,
        justifyContent:'center'
    },
    errorText: {
        color: 'red',
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    },
    terms: {
      color: '#F26349'
    }
  });