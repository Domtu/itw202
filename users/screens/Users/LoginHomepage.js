import React from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView } from "react-native";
import Action from "../../component/Components/Action";
import { FontAwesome5 } from '@expo/vector-icons';
const LoginHomepage = ({navigation}) => (
  <SafeAreaView style={{maxHeight:'100%'}}>



    
   
      <ScrollView>
        
        <View  style={{marginTop:10, backgroundColor:'#A2B9D1'}}>
        
          <View style={styles.container}>
               <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/break.png')} resizeMode="cover" style={styles.image}>
                  <Text style={styles.text}>BREAKFAST</Text>
                  <View style={{flexDirection:'row'}}>
                      <TouchableOpacity onPress={()=>navigation.replace("UserBreakfastItem")}>
                              <View style={{marginLeft:250,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("UserBreakFast")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
                  
                </ImageBackground>
           
          </View>

          <View style={styles.container}>
             <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/lunch.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>LUNCH</Text>
              <View style={{flexDirection:'row'}}>
                      <TouchableOpacity  onPress={()=>navigation.replace("UserLunchItem")}>
                              <View style={{marginLeft:250,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("UserLunch")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
            </ImageBackground>
            
            
          </View>

          <View style={styles.container}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/dinner.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>DINNER</Text>
              <View style={{flexDirection:'row'}}>
                      <TouchableOpacity onPress={()=>navigation.replace("UserDinnerItem")}>
                              <View style={{marginLeft:250,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("UserDinner")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
            </ImageBackground>
            
           
          </View>

          <View style={styles.container}>
           <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9, blurRadius:1}} source={require('../../assets/assets/snacks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>SNACKS</Text>
              <View style={{flexDirection:'row'}}>
                      <TouchableOpacity  onPress={()=>navigation.replace("UserSnacksItem")}>
                              <View style={{marginLeft:250,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("UserSnacks")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
            </ImageBackground>
           
            
          </View>

          <View style={styles.container}>
             <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/drinks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>Drinks</Text>
              <View style={{flexDirection:'row'}}>
                      <TouchableOpacity onPress={()=>navigation.replace("UserDrinksItem")}>
                              <View style={{marginLeft:250,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("UserDrinks")}>
                              <View style={{marginLeft:35,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
              
            </ImageBackground>
           
            
          </View>

          
        </View>
      </ScrollView>
      <View style={{width:'100%', height:'6%',backgroundColor:'#2f3d4f', flexDirection:'row'}} >
             <TouchableOpacity onPress={()=>navigation.replace("CommentCategory")}>
                      <View style={{marginLeft:100,
                                    marginTop:10}}      >
                      <FontAwesome5 name="comment" size={24} color="white" />
                        </View>
             </TouchableOpacity>
            
            <TouchableOpacity  onPress={()=>navigation.replace("Category")}>
                    <View style={{marginLeft:130,
                                  marginTop:10}}      >
                    <FontAwesome5 name="plus" size={24} color="white" />
                      </View>
           
            </TouchableOpacity>
               
           
          </View>

      
           
        
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
   marginTop: 5,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '95%',
    height: 150,
    marginLeft:20,
    overflow:'hidden',
  },
  text: {
    color: "white",
    fontSize: 28,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  
});

export default LoginHomepage ;