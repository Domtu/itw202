import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
import Action from "../../component/Components/Action";
import firebase from '../../component/Firebase/config';

export default function UserDrinks ({navigation}) {
  // const [tittle, setTittle] = useState(''); 
  const [drinks, setDrinks] = useState([]);
  const tittleRef = firebase.firestore().collection('drinks'); 

  useEffect(() => {

    tittleRef
        .orderBy('createdAt', 'desc')
        .onSnapshot(
            querySnapshot => {
                const newDrinks= []
                querySnapshot.forEach(doc => {
                    const tittle = doc.data()
                    tittle.id = doc.id
                    newDrinks.push(tittle)
                });
                setDrinks(newDrinks)
            },
            error => {

                console.error(error);
            }
        )
}, []);

const renderTittle = ({ item }) => {
  return (

      <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  )
}
return (

  <SafeAreaView>
   <View style={{backgroundColor:'#CFECEC', flexDirection:'row'}}>
     
      
        <TouchableOpacity onPress={()=>navigation.replace("LoginHomepage")}>
           
           <Image source={require('../../assets/assets/back.png')} style={{width:20,height:20,marginLeft:20,marginTop:40}}></Image>            
        </TouchableOpacity>
        <Text style={{marginLeft:110,marginTop:40, fontSize:17,color:'black'}}>Drinks/Comments</Text>
        </View>
    
    <View>

    {drinks.length > 0 && (
              <View style={styles.listContainer}>
                  <FlatList
                      data={drinks}
                      // numColumns={2}
                      horizontal={false}
                      ItemSeparatorComponent={() => <View style={{margin: 4}}/>}
                      renderItem={({ item }) => {
                        return(
                          <View>
                           
                           <View key={item.id} style={styles.place} >
                           <Text style={{marginTop:30, color:'black',marginLeft:130,fontSize:30}}>{item.comment}</Text>
                          
                           </View>
                           </View>
                         )
                       }} />
                
              </View>
          )}
    </View>
    <View>
    
    </View>
  </SafeAreaView>
)
}

const styles = StyleSheet.create({
  container: {
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '95%',
    height: 200,
    marginLeft:20,
    overflow:'hidden',
  },
  text: {
    color: "white",
    fontSize: 28,
    lineHeight: 84,
    fontWeight: "bold",
    marginTop:130
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  tittleContainer: {
    borderBottomColor: 'black',
    borderWidth: 1,
    flex:1 ,
    flexDirection: 'column',
    height: 100,
    width: 100,
    margin: 5,
    backgroundColor: 'blue'
  },
  tittleText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  listContainer: {
    height: '80%',
    width: 400,
    flexDirection: 'row',
    
  },
  place:{
  
  }
  
});



