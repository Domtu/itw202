import { StyleSheet, Text,Image,Keyboard,ScrollView, View,TouchableOpacity} from 'react-native'
import React, { useState } from 'react';
  import { Dropdown } from 'react-native-element-dropdown';
  import AntDesign from 'react-native-vector-icons/AntDesign';
  import UploadImage from '../../component/Components/Image';
  import firebase from '../../component/Firebase/config.js'
  import { SafeAreaView } from 'react-native-safe-area-context';
  import { TextInput } from 'react-native-paper';
  
  import ButtonOne from '../../component/Components/ButtonOne';


const AddBreakfast = ({navigation}) => {
  const [value, setValue] = useState(null);
  // recipes collection reference
  const [tittle, setTittle] = useState('');
  const [process, setProcess] = useState('');
  const [ingredient, setIngredient] = useState('');
  const [breakfast, setBreakfast] = useState([]);
  const tittleRef = firebase.firestore().collection('breakfast');
   


  
    const addBreakfast = () => {
      // check if we have a todo.
      if (tittle && tittle.length > 0) {
          // get the timestamp
          const timestamp = firebase.firestore.FieldValue.serverTimestamp();
          // structure the data  to save
          const data = {
              text: tittle,
              steps:process,
              needs:ingredient,
              createdAt: timestamp
          };
          // add the data to firestore db
          tittleRef
              .add(data)
              .then(() => {
                  // release todo state
                  setTittle('');
                  // release keyboard
                  Keyboard.dismiss();
                 
                navigation.navigate('LoginHomepage')  
              })
              .catch((error) => {
                  // show an alert in case of error
                  alert(error);
              })
              alert('Successfully added')
            }
          }
  
  return (
        
    <SafeAreaView style={{maxheight:'100%', backgroundColor:'white'}}>
        <ScrollView>
            <View>    
                    <View style={{flexDirection:'row',backgroundColor:'black',marginTop:10,}}>
                      <TouchableOpacity  onPress={()=>navigation.replace("LoginHomepage")}>
                      <Image  style={{width:30,height:40,marginTop:10,marginLeft:20}} source={require('../../assets/assets/back.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{marginLeft:80,marginTop:20,color:'white'}} >ADD RECIPES</Text>
                    </View>

                    <View  style={{width:800,height:800}}>
                      
                    <View style={{}}>
                    <View style={styles.view}>
                          <Text>Tittle</Text>
                          </View >
                      <View style={styles.input}>
                      <TextInput
                                numberOfLines={1}
                                multiline={true}
                                onChangeText={(text) => setTittle(text)}
                                    value={tittle}
                                   
                                style={{
                                    width:275, 
                                    borderWidth:1,
                                    borderColor:'black',
                                    backgroundColor:'white',
                                    marginLeft:20,
                                    marginBottom:5,
                                    textAlignVertical: "top",
                                   
                              }}
                            
                            />
                      </View>
                           
                    </View>
                     
                          
                      
                        
                      
                    
                    <View style={{flexDirection:'row'}}>
                        
                    
                    </View>
                    <View></View>
                    <View style={styles.view}>
                          <Text>Ingredients</Text>
                     </View>
                     <View style={styles.input}>
                     <TextInput 
                              numberOfLines={5}
                              multiline={true}
                              onChangeText={(steps) => setProcess(steps)}
                                    value={process}
                              style={{
                                width:275,
                                borderWidth:1,
                                borderColor:'black',
                                backgroundColor:'white',
                                marginLeft:20,
                                textAlignVertical: "top",
                            }}
                            
                            /> 
                     </View>
                           
                        
                   
                    <View style={styles.view}>
                          <Text>Process </Text>
                    </View>
                    <View style={styles.input}>
                    <TextInput
                                  numberOfLines={5}
                                  multiline={true}
                                  onChangeText={(needs) => setIngredient(needs)}
                                    value={ingredient}
                                  
                                    
                                    
                                  style={{
                                      width:275,
                                      borderWidth:1,
                                      borderColor:'black',
                                      backgroundColor:'white',
                                      margin:10,
                                      marginLeft:20,
                                      textAlignVertical: "top",
                                    
                                      
                                      
                                      
                                  }}
                            />
                            
                        
                    </View>
                    <View style={styles.view}>
                          <Text>Add Image</Text>
                     </View>
                           
                        
                   
                    <View style={styles.drop}>
                    
                        
                        <UploadImage></UploadImage>
                        
                    </View >
                    <View  style={{flexDirection:'row',}}>
                      <ButtonOne  onPress={()=>navigation.replace("LoginHomepage")}> CANCEL  </ButtonOne>
                      <ButtonOne  onPress={addBreakfast}>ADD </ButtonOne>
                      </View>
                    </View>
            </View>
            
            </ScrollView>
            </SafeAreaView>
          )
        }
        

const styles = StyleSheet.create({
    view:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:35,
        
        

    },
    input:{
      flexDirection:'row',
      marginTop:20,
      marginLeft:20,
      
    },
    drop:{
      flexDirection:'column'  
    },
    dropdown: {
      margin: 16,
      height: 50,
      borderBottomColor: 'gray',
      borderBottomWidth: 0.5,
    
    },
    icon: {
      marginRight: 5,
    },
      placeholderStyle: {
      fontSize: 16,
    },
    selectedTextStyle: {
      fontSize: 16,
    },
    iconStyle: {
      width: 20,
      height: 20,
    },
    inputSearchStyle: {
      height: 40,
      fontSize: 16,
    },
    
    
})
export default AddBreakfast
