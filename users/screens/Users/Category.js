import React from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView } from "react-native";
import Action from "../../component/Components/Action";
const CommentCategory = ({navigation}) => (
  <SafeAreaView style={{maxHeight:'100%'}}>


<View style={{flexDirection:'row',backgroundColor:'black',marginTop:40,}}>
                      <TouchableOpacity  onPress={()=>navigation.replace("LoginHomepage")}>
                      <Image  style={{width:30,height:40,marginTop:10,marginLeft:40}} source={require('../../assets/assets/back.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{marginLeft:80,marginTop:15,color:'white', fontSize:17}} >ADD RECIPES</Text>
                    </View>
    
   
      <ScrollView>
     

        
        <View  style={{marginTop:100}}>
        <Text style={{marginBottom:20,  marginLeft:110,fontSize:20}} > Choose a Category</Text>
        
          <View style={styles.container}>
            <TouchableOpacity  onPress={()=>navigation.replace("AddBreakfast")} >
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/break.png')} resizeMode="cover" style={styles.image}>
                  <Text style={{color: "white",
                          fontSize: 20,
                          lineHeight: 84,
                          fontWeight: "bold",
                        marginLeft:80
                          }}>BREAKFAST</Text>
                </ImageBackground>
            </TouchableOpacity>
           
          </View>

          <View style={styles.container}>
            <TouchableOpacity  onPress={()=>navigation.replace("AddLunch")}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/lunch.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>LUNCH</Text>
            </ImageBackground>
            </TouchableOpacity>
            
          </View>

          <View style={styles.container}>
            <TouchableOpacity onPress={()=>navigation.replace("AddDinner")}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/dinner.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>DINNER</Text>
            </ImageBackground>
            </TouchableOpacity>
           
          </View>

          <View style={styles.container}>
            <TouchableOpacity  onPress={()=>navigation.replace("AddSnacks")} >
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9, blurRadius:1}} source={require('../../assets/assets/snacks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>SNACKS</Text>
            </ImageBackground>
            </TouchableOpacity>
            
          </View>

          <View style={styles.container}>
            <TouchableOpacity onPress={()=>navigation.replace("AddDrinks")}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/drinks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>Drinks</Text>
              
            </ImageBackground>
            </TouchableOpacity>
            
          </View>

          
        </View>
      </ScrollView>

      
            {/* <View>
              <TouchableOpacity>
                <Image style={{width:30,height:30,marginLeft:40,marginTop:10}} source={require('../../assets/assets/h.png')}/>
                <Text style={{marginLeft:35,marginBottom:30}}>Home</Text>
                
              </TouchableOpacity>
            </View>

            <View>
              <TouchableOpacity  onPress={()=>navigation.replace("UserLoginScreen")}>
                <Image style={{width:30,height:30,marginLeft:90,marginTop:10}} source={require('../../assets/assets/a.png')} ></Image>
                <Text style={{marginLeft:80,}}>About</Text>
              </TouchableOpacity>
            </View>

            <View>
              <TouchableOpacity onPress={()=>navigation.replace("AddPage")}>
                
                
                           </TouchableOpacity>
            </View> */}
        
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
   marginTop: 7,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '80%',
    height: 80,
    marginLeft:20,
    overflow:'hidden',
    marginLeft:73
  },
  text: {
    color: "white",
    fontSize: 20,
    lineHeight: 84,
    fontWeight: "bold",
   marginLeft:100
    
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  
});

export default  CommentCategory ;