import React, {useState}from 'react';
import {View, StyleSheet, Text, TouchableOpacity,ScrollView, Image} from 'react-native';
import Header from '../../../component/Components/Header';
import TextInput from '../../../component/Components/TextInput'
import Button from '../../../component/Components/Button';
import Background from '../../../component/Components/Background';
import { emailValidator } from '../../../core/helpers/emailValidator';
import { passwordValidator } from '../../../core/helpers/passwordValidator';
import GoBack from '../../../component/Components/BackButton';
import { theme } from '../../../core/helpers/theme'; 
import { Formik } from 'formik';
import * as yup from 'yup';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Firebase from '../../../component/Firebase/config';
import BackButton from '../../../component/Components/BackButton';

const reviewSchema=yup.object({
    email:
    yup
    .string()
    .email('Please enter valid email')
    .required('Email is required'),
    password:
    yup
    .string()
    .required('Password is required'),
})

export default function LoginScreen({navigation}){
    const [emailError, setEmailError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)

    const storeData = async (value) => {
        try {
          await AsyncStorage.setItem('user', value) 
          navigation.navigate('LoginHomepage')
          console.log('gs') 
        } catch (e) {
          // saving error
        }
      }
    
      const LoginUser = async (values) => {
        Firebase.auth().signInWithEmailAndPassword(values.email, values.password)
        .then((userCredential) => {
          var user = userCredential.user;
          console.log(user.uid)
          storeData(user.uid)
          // ...
        })
    
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          var error = errorMessage.includes("password")
            console.log(errorMessage)
          if(error){
            setPasswordError('The Password is Invalid')        
            setEmailError(null)
            console.log(error)
          } else{
            setEmailError(`${values.email} is not registered email`)        
            setPasswordError(null)        
            console.log(error)
          }
          // ..
        });
      }
    return (
          <ScrollView>
          <View style={{width:'90%',marginLeft:15}}>
          <TouchableOpacity onPress={()=>navigation.replace("Page")}  >
        <Image  style={{width:30,height:20,marginTop:40,marginLeft:5}} source={require('../../../assets/assets/back.png')} ></Image>
      </TouchableOpacity>
            <View style={{marginLeft:120,marginTop:100}}>
            <Header>Sign In</Header>
            </View>
         
            <Formik
        validationSchema = {reviewSchema}
        initialValues={{
          email: '',
          password: '',
        }}
        onSubmit = {(values, actions) => {
        actions.resetForm()
        LoginUser(values)
        // console.log(values)
        }}
      >
      {(props)=>(
        <View style={styles.FormContainer}>
        
          <TextInput
            onChangeText = {props.handleChange('email')}
            value = {props.values.email}          
            label='Email'
          />
          {emailError ? 
            <Text style={styles.errorText}>{emailError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>
         
          <TextInput
            onChangeText = {props.handleChange('password')}
            value = {props.values.password}
            label='Password'
            secureTextEntry
            style={styles.input}
          />
          {passwordError ? 
            <Text style={styles.errorText}>{passwordError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>
          <View style={styles.forgotPassword}>
                <TouchableOpacity 
                    onPress={()=>navigation.navigate("ResetPasswordScreen")}>
                    <Text style={styles.forgot}> Forgot Your password? </Text>
                </TouchableOpacity>
            </View>

          <Button 
            mode='contained'
            onPress={props.handleSubmit}
          >Login</Button>
          <View
            style={{marginTop: 10}}>
              <View style={styles.row}>
                    <Text>Don't have an account? </Text>
                    <TouchableOpacity onPress={()=>navigation.replace("UserRegistrationScreen")}>
                        <Text style={styles.link}>  Sign up</Text>
                    </TouchableOpacity>
                </View>           
          </View>
        </View>
        )}
      
      </Formik>           
      </View>
      </ScrollView> 
        
    )
}

const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
        justifyContent:'center'
        
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    },
    forgotPassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.secondary,
    },
    errorText: {
        color: 'red',
      },
      FormContainer:{
        marginTop:80

      },
      input:{
        width:'100%'
      },
      link:{
        color:'blue',
      },
  });