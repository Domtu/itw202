import React, { useState } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import Button from "../../../component/Components/Button";
import Header from "../../../component/Components/Header";
import Background from "../../../component/Components/Background";
import TextInput from "../../../component/Components/TextInput";
import { emailValidator } from '../../../core/helpers/emailValidator';
import BackButton from "../../../component/Components/BackButton";
import { resetPassword } from "../../../component/Firebase/auth_api"

export default function ResetPasswordScreen({navigation}) {
    const [email, setEmail] = useState({ value: "", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async() => {
        const emailError = emailValidator(email.value);
        if (emailError) {
            setEmail({ ...email, error: emailError });
        }
        setLoading(true)

        const response = await resetPassword(email.value);

        if (response.error) {
            alert(response.error);
        }
        else {
            alert('Check your email for a reset link')
            navigation.navigate('UserLoginScreen') 
        }
        setLoading(false)
    }

    const onCancelPressed = () => {
        navigation.navigate('UserLoginScreen')
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Header>Restore Password</Header>
            <TextInput 
                label="Email" 
                value={email.value}
                placeholder='Enter email'
                autoCapitalize='none'
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({ value: text, error: "" })}
                description="Password reset email sent successfully"
            />
            <Button loading = {loading} mode="contained" onPress = {onSubmitPressed}>Submit</Button> 
            <Button mode="contained" onPress = {onCancelPressed}>Cancel</Button>
        </Background>
    )
}
