import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
import Action from "../../component/Components/Action";
import TextInput from "../../component/Components/TextInput";
import firebase from '../../component/Firebase/config';
import Icon from 'react-native-vector-icons/Ionicons';
export default function BreakfastItem ({navigation}) {
  const [tittle, setTittle] = useState('');
   
  const [drinks, setDrinks] = useState([]);
  const tittleRef = firebase.firestore().collection('drinks'); 

  useEffect(() => {

    tittleRef
        .orderBy('createdAt', 'desc')
        .onSnapshot(
            querySnapshot => {
                const newDrinks = []
                querySnapshot.forEach(doc => {
                    const tittle = doc.data()
                    tittle.id = doc.id
                    newDrinks.push(tittle)
                });
                setDrinks(newDrinks)
            },
            error => {

                console.error(error);
            }
        )
}, []);

const renderTittle = ({ item }) => {
    <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  return (

      <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  )
}
return (

  <SafeAreaView style={{backgroundColor:'#EEECED'}}>
      
      <View style={{backgroundColor:'#CFECEC', flexDirection:'row'}}>
     
      
        <TouchableOpacity onPress={()=>navigation.replace("Page")}>
           
           <Image source={require('../../assets/assets/back.png')} style={{width:20,height:20,marginLeft:20,marginTop:40}}></Image>            
        </TouchableOpacity>
        <Text style={{marginLeft:110,marginTop:40, fontSize:17,color:'#05445E'}}>Drinks</Text>
        </View>
        
      
    
    <View style={{height:'100%'}}>

    {drinks.length > 0 && (
              <View style={styles.listContainer}>
                  <FlatList
                      data={drinks}
                      renderItem={({ item }) => {
                        return(
                          <View>
                           
                           <View key={item.id} style={styles.place} >
                              <View >
                              <Text style={{ color:'black',marginLeft:10,fontSize:20,}}>{item.text}</Text>
                                      <ImageBackground  style={{width:30,height:30,marginTop:10,marginLeft:0,width:400,height:250,borderRadius:20}} source={require('../../assets/assets/Drink.png')}>
                                       
                           </ImageBackground>
                           <View style={{flexDirection:'row'}}>
                                    <ImageBackground source={require('../../assets/assets/comment.png')} style={{width:30,height:30,marginLeft:10,marginTop:10}}> 
                                    </ImageBackground>
                                    <Text style={{marginLeft:7,marginTop:10}}>Comments</Text>
                            </View>

                         <View style={{flexDirection:'row',borderColor:'black', }} >
                           
                          
                                    <Text style={{marginLeft:10,marginTop:20,color:'#05445E',fontSize:15}}>Ingredients</Text>
                                    <Text style={{ color:'black',marginLeft:-50,fontSize:15,marginTop:40}}>{item.needs}</Text>
                              </View>
                              <View style={{flexDirection:'column'}} >
                                    <Text style={{marginLeft:10,color:'#05445E',fontSize:15}}>Process</Text>
                                    <Text style={{ color:'black',marginLeft:30,fontSize:15,marginBottom:130}}>{item.steps}</Text>
                              </View>
                    
                           </View>

                                 
                                
                                
                                
                            </View>
                                
                          </View>
                          
                         )
                       }} />
                
              </View>
          )}
    </View>
  </SafeAreaView>
)
}

const styles = StyleSheet.create({
  container: {
   marginTop: 20,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '95%',
    height: 200,
    marginLeft:20,
    overflow:'hidden',
  },
  text: {
    color: "white",
    fontSize: 28,
    lineHeight: 84,
    fontWeight: "bold",
    marginTop:130
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  tittleContainer: {
    borderBottomColor: 'black',
    borderWidth: 1,
    flex:1 ,
    flexDirection: 'column',
    height: 100,
    width: 100,
    margin: 5,
    backgroundColor: 'blue'
  },
  tittleText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  listContainer: {
    height: '100%',
    width: 400,
    flexDirection: 'row',
    
  },
  place:{
        marginBottom:0
  }
  
});



