import { StyleSheet, Text,SafeAreaView,Image,TouchableOpacity, TextInput,Button, View } from 'react-native'
import React from 'react'
import { useState } from "react";

export default function Update({navigation}) {
    const [text, setText] = useState("");
    return (
        <View style={styles.container} >
            <TouchableOpacity onPress={()=>navigation.replace("LoginHomepage")} >
            <Image source={require('../../assets/assets/arrow_back.png')} style={styles.backArrowImage} />
            </TouchableOpacity>
                
                
                <Text style={styles.Text}>Profile Setting</Text>
         
            <View style={styles.container1}>
                        <Image source={require('../../assets/assets/recipe.png')} style={styles.profileImage} />
                        <TextInput
                        placeholder="Username"
                        style={styles.TextInput}
                    
                        />
                        <TextInput
                        placeholder="Email"
                        style={styles.TextInput}
                
                        />
                        <TextInput
                        placeholder="New password"
                        style={styles.TextInput}
                        />

                        <TextInput
                        placeholder="Confirm password"
                        style={styles.TextInput}
                        value={text}
                        onChangeText={(value) => setText(value)}
                        />
                <View style={{flexDirection:'row',marginLeft:100}}>
                        <View style={{marginRight:30}}>
                            <Button
                            title="Clear"
                                // style={styles.clearButton}
                                // onPress={() => setText("")}
                            />
                        </View>
                            <Button
                                title="Confirm"
                                style={{marginLeft:30}}         
                                />
                </View>
            </View> 
         </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"white",
        marginTop:25

    },
    container1:{
        marginTop:60,
        justifyContent:"center",
        backgroundColor:"white",
        alignItems:"center", 
        
    },
    Text:{
        fontSize:24,
        fontWeight:"bold",
        padding:15,
        marginLeft:100,
        marginTop:40
           
    },
   TextInput:{
        borderWidth: 1,
        width:280,
        height:40,
        padding:10,
        margin:5,
   },
   profileImage:{  
       paddingBottom:30,     
       width:80,
       height:90, 
   },
   backArrowImage: {
       height:30,
       width:30,
       marginTop:30,
       marginLeft:7
       
  },  
  backText:{
      marginLeft:5,
      fontSize:20,
      
      
  },
  confirmButton:{
      marginTop:30

  }
})



