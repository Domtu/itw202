import firebase from 'firebase/compat'
import 'firebase/compat/storage'
import firestore from 'firebase/compat/firestore'


const firebaseConfig = {
  apiKey: "AIzaSyAxfODBw-0hCanAf5LrK-H6y6PRIH6Wg-g",
  authDomain: "drukrecipes.firebaseapp.com",
  databaseURL: "https://drukrecipes-default-rtdb.firebaseio.com",
  projectId: "drukrecipes",
  storageBucket: "drukrecipes.appspot.com",
  messagingSenderId: "890136235460",
  appId: "1:890136235460:web:6964f6ff4b4c21f2eaa891",
  measurementId: "G-Z5BH01LWHM"
};
firebase.initializeApp(firebaseConfig);
// const storage=firebase.storage()
// const firestore=firebase.firestore()
// export {storage,firestore,firebase as default};
firebase.firestore();

export default firebase;

