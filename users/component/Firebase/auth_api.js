import firebase from '../Firebase/config'
import 'firebase/auth'
import { getAuth, sendPasswordResetEmail, signOut } from "firebase/auth";

export function logoutUser(){
    firebase.auth().signOut();
    
}

export async function resetPassword(email){
    try{
        await firebase
            .auth()
            .sendPasswordResetEmail(email);
        return {};
    }
    catch  (error){
        return{
            error: error.message,
        }
    }
}
