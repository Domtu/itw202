import React from 'react';

// import all the components we are going to use
import { SafeAreaView, StyleSheet,Image,  TouchableOpacity, View, Text } from 'react-native';

//Import ActionButton
import ActionButton from 'react-native-action-button';


//Import Icon for the ActionButton
import Icon from 'react-native-vector-icons/Ionicons';


export default function  Action  ({navigation})  {
  
  
 
 
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
      
        
        <ActionButton buttonColor="black" buttonMaxHeight='10%' >
          
          <ActionButton.Item
            buttonColor="black"
            title="search"
            
            // onPress={()=>navigation.replace("userUpdate")}
            >
            <Icon name="search" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item
          
            buttonColor="black"
            title="addRecipes"
            onPress={() => navigation.navigate("Category")}
            >
            
            <Icon name="add" style={styles.actionButtonIcon} />
            
            
          </ActionButton.Item>
         
        </ActionButton>
        
        
      </View>
     
    </SafeAreaView>
  );
  
};



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
   paddingRight:10,
    paddingBottom:67,
    width:150
    
  },
  titleStyle: {
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 10,
  },
  textStyle: {
    fontSize: 16,
    textAlign: 'center',
    padding: 10,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
    
  },
});
