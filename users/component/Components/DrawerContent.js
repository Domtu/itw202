import { StyleSheet, Text, View,Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { FontAwesome5 } from '@expo/vector-icons';

 const DrawerContent = ({navigation}) => {
  return (
     
    <View style={{backgroundColor:'white',width:'100%',marginTop:50, }}>
      <View style={{borderBottomColor:'black',borderBottomWidth:3,width:'100%',marginRight:40}}>
      <TouchableOpacity >
        <Image style={styles.admin}
          source={require('../../assets/assets/admin.png')}>
        </Image>  
       </TouchableOpacity>
      </View>
      
       <View style={{flexDirection:'row',marginTop:30,marginLeft:30}}>
      <TouchableOpacity style={{flexDirection:'row'}}>
      <FontAwesome5 name="home" size={24} color="black" />
        <Text style={{marginLeft:20, color:'black'}}>HOME</Text>
      </TouchableOpacity>
     </View>
       <View>
      {/* <TouchableOpacity style={{flexDirection:'row'}}>
        <Image style={styles.update}
          source={require('../../assets/assets/add.png')}>
        </Image>
        <Text style={{marginLeft:20,color:'black'}} onPress={()=>navigation.replace("Category")}>ADD RECIPES</Text>
      </TouchableOpacity> */}
      </View>
      <View>
      <TouchableOpacity style={{flexDirection:'row',marginTop:50,marginLeft:30}}>
      <FontAwesome5 name="user-alt" size={24} color="black" />
        <Text style={{marginLeft:20,color:'black'}} onPress={()=>navigation.replace("adminUpdate")}>UPDATE PROFILE</Text>
      </TouchableOpacity>
      </View>
      <View>
      <TouchableOpacity style={{flexDirection:'row',marginTop:50,marginLeft:30}}  onPress={()=>navigation.replace("User")}>
      <FontAwesome5 name="image" size={24} color="black" />
        <Text style={{marginLeft:20,color:'black'}}>MY POST</Text>
      </TouchableOpacity>
      </View>
  
      </View>
     
    
  )
}

export default DrawerContent;

const styles = StyleSheet.create({
  admin: {
    width:200,
    height:200,
    marginTop:45,
    marginLeft:20
  },
  home: {
    width:30,
    height:30,
    marginBottom:30,
    marginLeft:50

  },
  update: {
    width:30,
    height:30,
    marginBottom:30,
    marginLeft:50
  },
  logout: {
    width:30,
    height:30,
    marginBottom:30,
    marginLeft:50
  }
})