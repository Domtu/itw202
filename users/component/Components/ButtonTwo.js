import React from "react";
import { StyleSheet } from "react-native";
import { Button as PaperButton } from "react-native-paper";
import { theme } from '../../core/helpers/theme';

export default function ButtonOne({ mode, style, ...props }) {
    return (
        <PaperButton 
            style={[styles.button,
            mode === 'outlined' && { backgroundColor: theme.colors.surface },
            style,
        ]}
        labelStyle={styles.text}
        mode={mode}
        {...props}/>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '25%',
        marginVertical: 10,
        paddingVertical: 2,
        backgroundColor: 'black',
        marginLeft:80,
        marginTop:20

    },
    text: {
        fontWeight: 'bold',
        fontSize: 10,
        lineHeight: 26,
        color: 'white'
    },
})

