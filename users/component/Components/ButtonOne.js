import React from "react";
import { StyleSheet } from "react-native";
import { Button as PaperButton } from "react-native-paper";
import { theme } from '../../core/helpers/theme';

export default function ButtonOne({ mode, style, ...props }) {
    return (
        <PaperButton 
            style={[styles.button,
            mode === 'outlined' && { backgroundColor: theme.colors.surface },
            style,
        ]}
        labelStyle={styles.text}
        mode={mode}
        {...props}/>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '13%',
        marginVertical: 10,
        paddingVertical: 2,
        backgroundColor: 'black',
        marginLeft:60,
        marginTop:50

    },
    text: {
        fontWeight: 'bold',
        fontSize: 13,
        lineHeight: 26,
        color: 'white'
    },
})

