import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './components/funcComponents';

export default class App extends React.Component {
  render =() => (
    <View style={styles.container}>
      {/* <Text>Practical 2</Text>
      <Text>Stateless and statefull components</Text>
      <Text style={styles.text}> you are ready to start the journey</Text>
      <StatusBar style="auto" /> */}
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    marginTop:'5%'
  
  
    fontSize:24
  
}
});

