import { View, Text, StyleSheet, FlatList } from 'react-native'
import React from 'react'
import { Colors } from '../constants/Colors'
import PlaceItem from '../PlaceItem'

export default function PlacesList({places} ) {

    if(!places || places.length===0){
        return(
            <View style={styles.fallbackcontainer}>
                <Text style={styles.fallbacktext}>No places added yet, start adding some</Text>
            </View>
        )
    }

  return (
   <FlatList
   data={places}
   keyExtractor={(item)=>item.id}
   renderItem={({item}) => <PlaceItem place = {item}/>}
   />
  )
}

const styles = StyleSheet.create({
   fallbackcontainer: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center'
   },
fallbacktext: {
    fontSize: 16,
    color: Colors.primary200
}

})
