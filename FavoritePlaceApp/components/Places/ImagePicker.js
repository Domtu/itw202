import { StyleSheet, Text, View, Button, Image } from 'react-native'
import React, {useState} from 'react'
import { launchCameraAsync, cameraPermissionInformation, PermissionStatus } from 'expo-image-picker'
import { Colors } from '../constants/Colors';
import OutlineButton from '../UI/OutlineButton';


function ImagePicker () {

  const [pickedImage, setPickedImage] = useState();
  //const [cameraPermissionInformation, requestPermission] =

    async function takeImageHandler(){
        const image = await launchCameraAsync({
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5
        })
        //confirm.log(image);
        setPickedImage(image.uri);
    }
    let imagePreview = <Text>No image take yet</Text>

    if (pickedImage){
      imagePreview = <Image style={styles.image} source={{uri: pickedImage}} />
    }
  return (
    <View>
        <View style={styles.imagePreview}>{imagePreview}</View>
        <OutlineButton icon="camera" onPress={takeImageHandler} >Take Image</OutlineButton>
    </View>
  )
}

export default ImagePicker;

const styles = StyleSheet.create({
  imagePreview:{
    width: '100%',
    height: 200,
    marginVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderStartColor: Colors.primary100,
    borderRadius: 4
  },
  image:{
    width:'100%',
    height: '100%'
  }
})
