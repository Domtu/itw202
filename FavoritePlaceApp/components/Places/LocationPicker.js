import { StyleSheet, Text, View, Alert } from 'react-native'
import React from 'react'
import OutlineButton from '../UI/OutlineButton'
import { Colors } from '../constants/Colors'
import { getCurrentPositionAsync, useForegroundPermissions, PermissionStatus } from 'expo-location'


function LocationPicker() {

    const [locationPermissionInformation, requestPermission] = 
    useForegroundPermissions();

    async function verifyPermission(){
        if (
            locationPermissionInformation.status === PermissionStatus.UNDETERMINED
        ){
            const permissionResponse = await requestPermission();

            return permissionResponse.granted
        }

        if (
            locationPermissionInformation.status === PermissionStatus.DENIED
        ){
            Alert.alert(   
                'Insufficient Permission ',
                " you need grant location permission"
            )
        }
    }


    async function getLocationHandler(){
        const hasPermission = await verifyPermission();
        if(!hasPermission){
            return;
        }
        const location = await getCurrentPositionAsync();
        console.log(location)
    }

    function pickOnMapHandler(){}

  return (
    <View>
      <View style={styles.mapPreview}>
          <View style={styles.actions}>
              <OutlineButton icon={"location" } onPress={getLocationHandler}>
                  Locate user
              </OutlineButton>

              <OutlineButton icon={'map'} onPress={pickOnMapHandler}>
                  Pick on map
              </OutlineButton>
          </View>
      </View>
    </View>
  )
}

export default LocationPicker

const styles = StyleSheet.create({
    mapPreview: {
        width: '100%',
        height: 200,
        marginVertical: 8,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: Colors.primary100,
        borderRadius: 4
    },

    actions:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    }
})
