import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import IconButton from './components/UI/IconButton';
import { Colors } from './components/constants/Colors';
import AllPlaces from './Screens/AllPlaces';
import AddPlace from './Screens/AddPlace';

export default function App() {

  const Stack = createNativeStackNavigator();

  return (
    <>
    <StatusBar style='dark' />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {backgroundColor: Colors.primary500},
            headerTintColor: Colors.gray700,
            contentStyle: {backgroundColor: Colors.gray700}
          }}
        >
          <Stack.Screen 
              name='AllPlaces'
               component={AllPlaces}
              options={({navigation})=> ({
                title: 'Your fav place',
                headerRight: ({tintColor}) =>(
                  <IconButton
                    icon='add'
                    size={30}
                    color={tintColor}
                    onPress = {() => navigation.navigate('AddPlace')}
                  />

                )
              })}
              />
          <Stack.Screen
           name='AddPlace'
            component={AddPlace}
            options= {{
              title: 'Add a New Place'
            }}/>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
