import React from "react";
import { Component } from "react";
import {Text, View, Button} from 'react-native'
class CountClass extends Component{
    state ={ count:0}
    onPress = ()=>{
        this.setState({
        count: this.state.count + 1 
        })
    }
    render (){
        return(
            <View><Text>You clicked {this.state.count} times</Text>
            <Button 
            onPress={this.onPress}title="count"/>
            </View>
        )
    }
}
export default CountClass;