import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import CountClass from './Component/CountClass';
export default function App() {
  return (
    <View style={styles.container}>
      {/* <Text>Open up App.js to tart working on your app!</Text> */}
      <StatusBar style="auto" />
      {/* <Courses></Courses> */}
      <CountClass/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
