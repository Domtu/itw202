// import { firebase } from '@firebase/app'

// const firebaseConfig = {
//   apiKey: "AIzaSyAKRrxaYN8Io7JqFR52hkMVpb28yASzx6A",
//   authDomain: "practical-eea60.firebaseapp.com",
//   databaseURL: "https://practical-eea60-default-rtdb.firebaseio.com",
//   projectId: "practical-eea60",
//   storageBucket: "practical-eea60.appspot.com",
//   messagingSenderId: "430754133044",
//   appId: "1:430754133044:web:c7f1e3ae8f60c64e338661",
//   measurementId: "G-5TNGJ5X3L0"
// };

// firebase.initializeApp(CONFIG)

// export default firebase;


import { firebase } from '@firebase/app'

const CONFIG= {
  apiKey: "AIzaSyAKRrxaYN8Io7JqFR52hkMVpb28yASzx6A",
  authDomain: "practical-eea60.firebaseapp.com",
  databaseURL: "https://practical-eea60-default-rtdb.firebaseio.com",
  projectId: "practical-eea60",
  storageBucket: "practical-eea60.appspot.com",
  messagingSenderId: "430754133044",
  appId: "1:430754133044:web:c7f1e3ae8f60c64e338661",
  measurementId: "G-5TNGJ5X3L0"
};

firebase.initializeApp(CONFIG);

export default firebase;

