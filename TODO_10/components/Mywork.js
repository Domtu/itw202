import { StyleSheet, Text, View,TextInput} from 'react-native'
import React,{useState} from 'react'



const Mytext = () => {

const [text, setText]=useState('What is your name?');

  return (
      <View>
        <Text>{text}</Text>
        <TextInput
            style={{
                width:300,
                height:50,
                borderWidth:1,
                borderColor:'grey',
                backgroundColor:'grey'
            }}
            onChangeText={(nam)=>setText(<Text>Hi {nam} from Gyalpozing College of Infomation Technology</Text>)}
        />
   
    </View>
  )
}

export default Mytext

const styles = StyleSheet.create({})