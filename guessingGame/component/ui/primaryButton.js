import React from 'react';
import { View, Text, Pressable, StyleSheet } from 'react-native';
import Colors from '../../constants/Colors.ios';

const Primarybutton = ({children, onPress}) => {
    return (
        <View style={styles.buttonOuterContainer}>
          <Pressable android_ripple={{color: Colors.primary600}} 
          style={({pressed}) => pressed ?
          [styles.pressed, styles.buttonInnerContainer]: styles.buttonInnerContainer}
          onPress = {onPress}
          >
       
           <Text style = {styles.buttonText}>{children}</Text>
       
          </Pressable>
       </View>
    );
}

export default Primarybutton;

const styles = StyleSheet.create({
    buttonInnerContainer: {
        backgroundColor:Colors.primary500,
        borderRadius: 28,
        paddingVertical:8,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.25,
        margin: 4,
        elevation: 4,
        

    },
    buttonText:{
        color: 'white',
        textAlign: 'center'
    },
    buttonOuterContainer:{
        borderRadius: 28,
        margin: 4,
        overflow:'hidden'
    },
    pressed:{
        opacity:0.75,
    }
})