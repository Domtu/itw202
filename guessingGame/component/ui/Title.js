import React from 'react'
import { View, Text, StyleSheet } from 'react-native' 
import Colors from '../../constants/Colors'

export default function Title({children}) {
  return (
    <View>
        <Text style={Styles.text}>{children}</Text>
    </View>
  )
}
const Styles = StyleSheet.create({
    text:{
      
        borderWidth: 2,
        borderColor: Colors.basic,
        padding: '5%',
        color: Colors.basic,
        fontSize: 24,
        textAlign:'center',
        maxWidth:'80%'
      }
})
