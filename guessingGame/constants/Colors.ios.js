const Colors = {
    primary500: 'red',
    primary600: '#640233',
    primary700: '#4c0329',
    primary800: '#3b021f',
    accent500: '#ddb52f',
    basic: '#ffffff'
}
export default Colors;