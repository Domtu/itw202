import React, {useState} from 'react'
import {View, TextInput, StyleSheet, Alert, Dimensions, Text, useWindowDimensions, KeyboardAvoidingView} from 'react-native';
import { ScrollView } from 'react-native';

import Card from '../component/ui/Card';
import InstructionText from '../component/ui/InstructionText';
import Primarybutton from '../component/ui/primaryButton';
import Title from '../component/ui/Title';
import Colors from '../constants/Colors';

export default function StartGameScreen({onPickNumber}) {
  const [enteredNumber, setEnteredNumber] = useState('');

  const {width, height} = useWindowDimensions();
  function numberInputHandler(enteredText) {
    setEnteredNumber(enteredText);

  } 
  function resetInputHandler() {
    setEnteredNumber('');
  } 
  function confirmInputHandler() {
    const chosenNumber = parseInt(enteredNumber)
      if (isNaN(chosenNumber ) || chosenNumber < 1 || chosenNumber > 99) {
        Alert.alert('Invalid Number! Number has to be a number between 1 and 99',
        [{text: 'okay', style: 'destructive', onPress: resetInputHandler}])
        return;
      } 
      //  console.log(chosenNumber) 
      onPickNumber(chosenNumber)
      }    
      const marginTopDistance = height < 380 ? 30:100;
  return (
    <ScrollView style ={styles.screen}>
    <KeyboardAvoidingView style = {styles.screen} behavior='position'>
    <View style={[styles.rootContainer, {marginTop: marginTopDistance}]}>
      <Title> Guess My Number</Title>
    <Card style={styles.inputContainer}>
      <InstructionText> Enter a Number </InstructionText>
        <TextInput 
        maxLength={2} 
        keyboardType={'number-pad'}
        autoCapitalize='none'
        autoCorrect={false}
        value = {enteredNumber}
        onChangeText={numberInputHandler}
        style={styles.numberInput}
        />
        <View style= {styles.buttonsContainer}>
        <View style= {styles.buttonContainer}>
        <Primarybutton onPress = {resetInputHandler}>Reset</Primarybutton>
        </View>
        <View style= {styles.buttonContainer}>
        <Primarybutton onPress = {confirmInputHandler}>Confirm</Primarybutton>
        </View>
        </View>
    </Card>
    </View>
    </KeyboardAvoidingView>
    </ScrollView>
    )
}
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  screen:{
    flex: 1
  },
  inputContainer: {
    marginTop: 36,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: Colors.primary500,
    borderRadius: 8,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25,
    elevation: 1,
    justifyContent:'center',
    alignItems:'center'
  },
  instructionText:{
    color:Colors.accent500,
    fontSize:24
  },
  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor: Colors.accent500,
    borderBottomWidth: 2,
    textAlign: 'center',
    color: Colors.accent500,
    marginVertical: 8,
    fontWeight:'bold',
    
  },
  buttonsContainer:{
    flexDirection: 'row'
  },
  buttonContainer:{
    flex: 1
  },
  rootContainer:{
    flex: 1,
    marginTop: deviceHeight < 380 ? 30:100,
    alignItems: 'center'
  }
})
