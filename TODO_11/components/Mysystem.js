import { setStatusBarNetworkActivityIndicatorVisible } from 'expo-status-bar';
import React, { useState } from 'react';
import { Pressable, StyleSheet, Text, TouchableOpacityBase, View} from 'react-native';

const BTNComponent = () => {
  const [timesPressed, setTimesPressed] = useState(0);
  const [isDisabled, setIsDisabled] = useState(false);

  let textLog = "The button isn't pressed yet ";
  if (timesPressed > 0) {
    textLog = 'The button was pressed ' + timesPressed + ' times!';
  }

  const disableBTN = () => {
    if( timesPressed == 3) {
        setIsDisabled(true)
    }
    else {
        setTimesPressed((current) => current + 1)
    }
}
  return (
    <View style={styles.container}>
      <View style={styles.logBox}>
        <Text testID="pressable_press_console">{textLog}</Text>
      </View>
      <Pressable 
        onPress={disableBTN }
        style={({ pressed }) => [
          {
            backgroundColor: pressed
              ? 'lightblue'
              : 'lightblue'
          },
          styles.wrapperCustom
        ]}>

        {({ pressed }) => (
          <Text style={styles.text}>
            {pressed ? 'Pressed!' : 'Press Me'}
          </Text>
        )}
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    width: 350,
    marginBottom: 200,
  },
  text: {
    fontSize: 16,
  },
  wrapperCustom: {
    padding: 10,

    alignItems:'center'
  },
  logBox: {
    padding: 20,
    backgroundColor: 'white'
  }

  }
});
export default BTNComponent;
