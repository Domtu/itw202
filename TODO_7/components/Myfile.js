import React from "react";
import {StyleSheet, Text, View} from 'react-native';

export default function Myfile(){
    return(
        <View style={styles.container}>
            <View style={styles.square1}/>
            
            <View style={styles.square2}/>
           
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'flex-start',
        justifyContent:'center',
    },
    square1:{
        width:200,
        height:200,
        backgroundColor: 'red',
        justifyContent:'center',
        alignItems:'center'

    },
    square2:{
        width:200,
        height:200,
        backgroundColor: 'blue',
        justifyContent:'center',
        alignItems:'center'


    },

});