import { StyleSheet, Text, View } from 'react-native';
import Mycomponent from './components/Mycomponent';

export default function App() {
  return (
    <View style={styles.container}>
     <Mycomponent />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
