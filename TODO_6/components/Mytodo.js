import React from "react";
import {StyleSheet, Text, View} from 'react-native';

export default function Apps(){
    return(
        <View style={styles.container}>
            <View style={styles.rectangle1}>
                 <Text>1</Text>
            </View>
            <View style={styles.rectangle2}>
                <Text>2</Text>
            </View>
            
            <View style={styles.rectangle3}>
                <Text>3</Text>
            </View>
            
            
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'flex-start',
        justifyContent:'center',
        flexDirection: 'row',
        marginTop:100,
        marginRight:100
    },
    rectangle1:{
        width:50,
        height:200,
        backgroundColor: 'red',
        justifyContent:'center',
        alignItems:'center'

    },
    rectangle2:{
        width:80,
        height:200,
        backgroundColor: 'blue',
        justifyContent:'center',
        alignItems:'center'


    },
    rectangle3:{
        width:10,
        height:200,
        backgroundColor: 'green',
        justifyContent:'center'

    },
});