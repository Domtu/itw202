import React from "react";
import { ActivityIndicator } from "react-native-paper";
import Background from "../components/Background";
import firebase from "firebase/app";

export default function AuthLoadingScreen({ navigation }) {
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            //User logged in
            navigation.reset({
                routes: [{ name: "HomeScreen"}],
            });
        }else {
            //user is not logged in
            navigation.reset({
                routes: [{ name: "StartScreen" }],
            });
        }
    });
    return (
        <Background>
            <ActivityIndicator size="large" />
        </Background>
    )
}