import React from "react";
import { View, StyleSheet } from "react-native";
import { Paragraph } from "react-native-paper";
import Button from "../components/Button";
import Header from "../components/Header";
import Background from "../components/Background";
import Logo from "../components/Logo";

export default function StartScreen({navigation}) {
    return (
        <Background>
            <Logo />
            <Header>Login Templete</Header>
            <Paragraph>
                    The easitest way to start with your amazing application.
            </Paragraph>
            <Button mode="outlined" 
                onPress = {() => {
                    navigation.navigate("LoginScreen")
                }}>Login</Button>
            <Button 
                mode="contained"
                onPress = {() => {
                    navigation.navigate("RegisterScreen")
                }}>Sign Up</Button>
        </Background>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    }
})