import React from "react";
import { View, StyleSheet, Text} from 'react-native';
import { theme } from "../core/theme";
import { TextInput as Input } from "react-native-paper";

export default function TextInput({ errorText, description, ...props }) {
    return (
        <View style={styles.container}>
            <Input
                style={styles.input}
                selectionColor={theme.colors.primary}
                underlineColor="transparent"
                mode="outlined"
                {...props} 
            />
            {description && !errorText ? (
                <Text style={styles.error}>{errorText}</Text>
                ) : null}
            {errorText ? <Text style={styles.error}>{description}</Text> : null}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 12

    },
    input: {
        backgroundColor: theme.colors.surface,
    },
    description: {
        fontSize: 13,
        color: theme.colors.secondary,
        padding: 8,
    },
    error: {
        fontSize: 13,
        color: theme.colors.error,
        padding: 8,
    }
})